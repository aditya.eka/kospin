<div class="leftpanel">
    <div class="media profile-left">
        <a class="pull-left profile-thumb" href="profile.html">
            <img class="img-circle" src="{{asset('assets')}}/images/photos/adit.jpg" alt="">
        </a>
        <div class="media-body">
            <h2 class="media-heading">Aditya Eka Arifyanto</h2>
            <small class="text-muted">Superuser</small>
        </div>
    </div><!-- media -->

    <h5 class="leftpanel-title">Navigation</h5>
    <ul class="nav nav-pills nav-stacked">
        <li><a href="index.html"><i class="fa fa-home"></i> <span>Beranda</span></a></li>
        <li class="parent"><a href=""><i class="fa fa-share-square-o"></i> <span>Transaksi Kas</span></a>
            <ul class="children">
                <li><a href="alerts.html"><i class="fa fa-circle-o"></i> <span>Pemasukan</span></a></li>
                <li><a href="buttons.html"><i class="fa fa-circle-o"></i> <span>Pengeluaran</span></a></li>
                <li><a href="extras.html"><i class="fa fa-circle-o"></i> <span>Transfer</span></a></li>
            </ul>
        </li>
        <li class="parent"><a href=""><i class="fa fa-bank"></i> <span>Simpanan</span></a>
            <ul class="children">
                <li><a href="alerts.html"><i class="fa fa-circle-o"></i> <span>Setor Tunai</span></a></li>
                <li><a href="buttons.html"><i class="fa fa-circle-o"></i> <span>Tarik Tunai</span></a></li>
            </ul>
        </li>
        <li class="parent"><a href=""><i class="fa fa-random"></i> <span>Pinjaman</span></a>
            <ul class="children">
                <li><a href="alerts.html"><i class="fa fa-circle-o"></i> <span>Data Pengajuan</span></a></li>
                <li><a href="buttons.html"><i class="fa fa-circle-o"></i> <span>Data Pinjaman</span></a></li>
                <li><a href="buttons.html"><i class="fa fa-circle-o"></i> <span>Bayar Angsuran</span></a></li>
                <li><a href="buttons.html"><i class="fa fa-circle-o"></i> <span>Pinjaman Lunas</span></a></li>
            </ul>
        </li>
        <li class="parent"><a href=""><i class="fa fa-file-text"></i> <span>Laporan</span></a>
            <ul class="children">
                <li><a href="alerts.html"><i class="fa fa-circle-o"></i> <span>Data Anggota</span></a></li>
                <li><a href="buttons.html"><i class="fa fa-circle-o"></i> <span>Kas Anggota</span></a></li>
                <li><a href="buttons.html"><i class="fa fa-circle-o"></i> <span>Jatuh Tempo</span></a></li>
                <li><a href="buttons.html"><i class="fa fa-circle-o"></i> <span>Kredit Macet</span></a></li>
                <li><a href="buttons.html"><i class="fa fa-circle-o"></i> <span>Transaksi Kas</span></a></li>
                <li><a href="buttons.html"><i class="fa fa-circle-o"></i> <span>Buku Besar</span></a></li>
                <li><a href="buttons.html"><i class="fa fa-circle-o"></i> <span>Neraca Saldo</span></a></li>
                <li><a href="buttons.html"><i class="fa fa-circle-o"></i> <span>Kas Simpanan</span></a></li>
                <li><a href="buttons.html"><i class="fa fa-circle-o"></i> <span>Kas Pinjaman</span></a></li>
                <li><a href="buttons.html"><i class="fa fa-circle-o"></i> <span>Saldo Kas</span></a></li>
                <li><a href="buttons.html"><i class="fa fa-circle-o"></i> <span>Laba Rugi</span></a></li>
                <li><a href="buttons.html"><i class="fa fa-circle-o"></i> <span>SHU</span></a></li>
            </ul>
        </li>
        <li class="parent"><a href=""><i class="fa fa-database"></i> <span>Master Data</span></a>
            <ul class="children">
                <li><a href="alerts.html"><i class="fa fa-circle-o"></i> <span>Jenis Simpanan</span></a></li>
                <li><a href="buttons.html"><i class="fa fa-circle-o"></i> <span>Jenis Akun</span></a></li>
                <li><a href="buttons.html"><i class="fa fa-circle-o"></i> <span>Data Kas</span></a></li>
                <li><a href="buttons.html"><i class="fa fa-circle-o"></i> <span>Lama Angsuran</span></a></li>
                <li><a href="buttons.html"><i class="fa fa-circle-o"></i> <span>Data Barang</span></a></li>
                <li><a href="buttons.html"><i class="fa fa-circle-o"></i> <span>Data Anggota</span></a></li>
            </ul>
        </li>
        <li class="parent"><a href=""><i class="fa fa-cogs"></i> <span>Setting</span></a>
            <ul class="children">
                <li><a href="alerts.html"><i class="fa fa-circle-o"></i> <span>Identitas Koperasi</span></a></li>
                <li><a href="buttons.html"><i class="fa fa-circle-o"></i> <span>Suku Bunga</span></a></li>
            </ul>
        </li>

    </ul>
    <br>
    <ul>
        <div class="container pull-left">
            <?php
            $today = date('d-M-Y');
            ?>
            <i class="fa fa-calendar"></i> <span><?= $today; ?></span> <br>
            <?php $today = getdate(); ?>
            <script>
                var d = new Date(Date.UTC(<?php echo $today['year'] . "," . $today['mon'] . "," . $today['mday'] . "," . $today['hours'] . "," . $today['minutes'] . "," . $today['seconds']; ?>));
                setInterval(function() {
                    d.setSeconds(d.getSeconds() + 1);
                    $('#timer').text((d.getHours() + ':' + d.getMinutes() + ':' + d.getSeconds()));
                }, 1000);
            </script>
            <i class="fa fa-clock-o"></i> <span><label id="timer"></label></span>
        </div>
    </ul>

</div><!-- leftpanel -->