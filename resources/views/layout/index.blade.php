<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SIMKOS by ADICT</title>

    <link href="{{asset('assets')}}/css/style.default.css" rel="stylesheet">
    <link href="{{asset('assets')}}/css/bootstrap.min.css" rel="stylesheet">
    <link href="{{asset('assets')}}/css/font-awesome.min.css" rel="stylesheet">
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
        <script src="js/html5shiv.js"></script>
        <script src="js/respond.min.js"></script>
        <![endif]-->

</head>

<body>

    <header>
        <div class="headerwrapper">
            <div class="header-left">
                <a href="#" class="logo">
                    <!-- <img src="{{asset('assets')}}/images/logo-primary.png" alt="" /> -->
                    <h4 style="color:white;">SIMKOS BY ADICT</h4>
                </a>
                <div class="pull-right">
                    <a href="" class="menu-collapse">
                        <i class="fa fa-bars"></i>
                    </a>
                </div>
            </div><!-- header-left -->

            @include('layout/right-header')

        </div><!-- headerwrapper -->
    </header>

    <section>
        <div class="mainwrapper">
            @include('layout/sidebar')

            <div class="mainpanel">
                <div class="pageheader">
                    <div class="media">
                        <div class="pageicon pull-left">
                            <i class="fa fa-home"></i>
                        </div>
                        <div class="media-body">
                            <ul class="breadcrumb">
                                <li><a href=""><i class="glyphicon glyphicon-home"></i></a></li>
                                <li><a href="">Pages</a></li>
                                <li>Blank Page</li>
                            </ul>
                            <h4>Blank Page</h4>
                        </div>
                    </div><!-- media -->
                </div><!-- pageheader -->

                <div class="contentpanel">

                    <!-- CONTENT GOES HERE -->

                </div><!-- contentpanel -->

            </div>
        </div><!-- mainwrapper -->
    </section>


    <script src="{{url('assets')}}/js/jquery-1.11.1.min.js"></script>
    <script src="{{url('assets')}}/js/jquery-migrate-1.2.1.min.js"></script>
    <script src="{{url('assets')}}/js/bootstrap.min.js"></script>
    <script src="{{url('assets')}}/js/modernizr.min.js"></script>
    <script src="{{url('assets')}}/js/pace.min.js"></script>
    <script src="{{url('assets')}}/js/retina.min.js"></script>
    <script src="{{url('assets')}}/js/jquery.cookies.js"></script>

    <script src="{{url('assets')}}/js/custom.js"></script>

</body>

</html>